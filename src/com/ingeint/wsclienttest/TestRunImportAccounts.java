package com.ingeint.wsclienttest;

import org.idempiere.webservice.client.base.ParamValues;
import org.idempiere.webservice.client.base.Enums.WebServiceResponseStatus;
import org.idempiere.webservice.client.net.WebServiceConnection;
import org.idempiere.webservice.client.request.RunProcessRequest;
import org.idempiere.webservice.client.response.RunProcessResponse;

public class TestRunImportAccounts extends AbstractTestWS {

	
	@Override
	public String getWebServiceType() {
		return "RunProcessImportAccounts";
	}

	@Override
	public void testPerformed() {
		RunProcessRequest process = new RunProcessRequest();
		process.setWebServiceType(getWebServiceType());
		process.setLogin(getLogin());

		ParamValues params = new ParamValues();
		params.addField("AD_Client_ID", "11");
		params.addField("C_Element_ID", "105");
		process.setParamValues(params);

		WebServiceConnection client = getClient();

		try {
			RunProcessResponse response = client.sendRequest(process);

			if (response.getStatus() == WebServiceResponseStatus.Error) {
				System.out.println(response.getErrorMessage());
			} else {
				System.out.println(response.getSummary());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
